<?php
/**
 * @file
 * A module for viewing a syslog stored in a MySQL database
 */

/**
 * Display help and module information
 * @param $path
 *   Which path of the site we're displaying help
 * @param $arg
 *  Array that holds the current path as would be returned from arg() function
 * @return
 *  Help text for the path
 */
function syslog_viewer_help($path, $arg) {
  $output = '';  // Declare output variable
  switch ($path) {
    case "admin/help#syslog_viewer":
      $output = '<p>'.  t("FIXME!!!!") .'</p>';
      break;
  }
  return $output;
}

/**
 * Valid permissions for this module
 * @return array
 *   An array of valid permissions for the syslog_viewer module
 */
function syslog_viewer_perm() {
  return array('view syslog_viewer data', 'administer syslog_viewer');
}

/**
 * Admin page callback
 * @return
 *   Themed page
 */
function syslog_viewer_admin() {
  $form = array(); // Declare $form array

  // Which database to use
  $form['syslog_viewer_database'] = array(
    '#type' => 'textfield',
    '#title' => t('Database containing syslog table'),
    '#default_value' => variable_get('syslog_viewer_database', 'default'),
    '#size' => 20,
    '#maxlength' => 50,
    '#description' => t("Database connection for the syslog database. See README for more info."),
    '#required' => TRUE,
  );

  // Which table in that database to use
  $form['syslog_viewer_table'] = array(
    '#type' => 'textfield',
    '#title' => t('Syslog table'),
    '#default_value' => variable_get('syslog_viewer_table', 'systemlogs'),
    '#size' => 20,
    '#maxlength' => 50,
    '#description' => t("Table containing the syslog. N.B. see README for supported schema"),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}

/**
 * Stub admin page validation function
 * @param form
 * @param form_state
 */
function syslog_viewer_admin_validate() {
}

/**
 * Internal function to create links for generating 'WHERE' clauses
 * @param $type
 *   One of 'hostname', 'facility', 'severity, 'program'
 * @param $data
 *  Value for 'WHERE' clause
 * @return
 *  Themed link (i.e. l())
 */
function _syslog_viewer_get_where_link($type, $data) {
  switch ($type) {
    case 'hostname':  //NOTE: put an if(!isset($_G['foo']) { return $bar } else { return $data } here :)
      if (!isset($_GET['hostname'])) {
        return l($data, $_GET['q'], array('query' => drupal_query_string_encode($_GET, array('q', 'hostname'))
                                                   .'&hostname='. $data));
      }
      else {
        return $data;
      }
      break;
    case 'facility':
      if (!isset($_GET['facility'])) {
        return l($data, $_GET['q'], array('query' => drupal_query_string_encode($_GET, array('q', 'facility'))
                                                   .'&facility='. $data));
      }
      else {
        return $data;
      }
      break;
    case 'severity':
      if (!isset($_GET['severity'])) {
        return l($data, $_GET['q'], array('query' => drupal_query_string_encode($_GET, array('q', 'severity'))
                                                   .'&severity='. $data));
      }
      else {
        return $data;
      }
      break;
    case 'program' :
      if (!isset($_GET['program'])) {
        return l($data, $_GET['q'], array('query' => drupal_query_string_encode($_GET, array('q', 'program' ))
                                                   .'&program='. $data));
      }
      else {
        return $data;
      }
      break;
  }
}

/**
 * Internal function to craft the necessary WHERE clause for filtering
 * by host, program etc.
 * @return
 *   SQL WHERE clause to append to SQL query
 */
function _syslog_viewer_get_where_clause() {
  $expressions = array(); // Declare array of sub-expressions to be concatenated together
  $clause = ''; // Declare varible for final output
  if (isset($_GET['hostname'])) {
    $expressions[] = '`hostname` = \''. db_escape_string($_GET['hostname']) .'\' ';
  }
  if (isset($_GET['facility'])) {
    $expressions[] = '`facility` = \''. db_escape_string($_GET['facility']) .'\' ';
  }
  if (isset($_GET['severity'])) {
    $expressions[] = '`severity` = \''. db_escape_string($_GET['severity']) .'\' ';
  }
  if (isset($_GET['program'])) {
    $expressions[] = '`program`  = \''. db_escape_string($_GET['program'])  .'\' ';
  }

  $count = count($expressions);
  if ($count == 0) {
    // Return blank
    return '';
  }
  elseif ($count == 1) {
    // If we only have one, this is easy
    return ' WHERE '. $expressions[0];
  }
  else {
    $clause = ' WHERE '; // Rogue spaces don't hurt, lack of them does
    for ($i = 0; $i < $count; $i++) {
      $clause .= $expressions[$i];
      if ($i != $count - 1) {
        // If not final insert AND
        $clause .= 'AND ';
      }
    }
    return $clause;
  }
}

/**
 * Simple helper function to remove sortable link when list is filtered
 * @param $field
 *   The name of the field that would otherwise have been output
 * @return
 *   Either a field name, or a NULL
 */
function _syslog_viewer_sortable_link($field) {
  if (!isset($_GET[$field])) {
    return $field;
  }
  else {
    return NULL;
  }
}

/**
 * Helper function to create text for the top of the syslog page
 * @return
 *  Themes text
 */
function _syslog_viewer_display_text() {
  $output = ''; // Declare output variable
  $title  = ''; // Declare variable for storing new page title

  // Ignore the coder.module moan here, this is neater
  $output .= t('Syslog data pulled from the database. Note: this may not be completly up to date'
          .    'due to delays in the syslogd updating the database.'                             );

  // Staging varible for the breadcrumb
  $breadcrumb = array(
    l('All', $_GET['q'], array(
      'query' => drupal_query_string_encode($_GET, array(
        'q', 'hostname', 'facility', 'severity', 'program')
      )
    ))
  );
  // If at least one filter is in use, change the title
  if (isset($_GET['hostname']) or isset($_GET['facility']) or isset($_GET['severity']) or isset($_GET['program'])) {
    if (isset($_GET['hostname'])) {
      $breadcrumb[] = l($_GET['hostname'], $_GET['q'], array(
        'query' => drupal_query_string_encode($_GET, array('q', 'hostname')) .'&hostname='. $_GET['hostname'])
      );
      $title .= ' » '. $_GET['hostname'] .' ';
    }
    if (isset($_GET['facility'])) {
      $breadcrumb[] = l($_GET['facility'], $_GET['q'], array(
        'query' => drupal_query_string_encode($_GET, array('q', 'facility')) .'&facility='. $_GET['facility'])
      );
      $title .= ' » '. $_GET['facility'] .' ';
    }
    if (isset($_GET['severity'])) {
      $breadcrumb[] = l($_GET['severity'], $_GET['q'], array(
        'query' => drupal_query_string_encode($_GET, array('q', 'severity')) .'&severity='. $_GET['severity'])
      );
      $title .= ' » '. $_GET['severity'] .' ';
    }
    if (isset($_GET['program'])) {
      $breadcrumb[] = l($_GET['program'], $_GET['q'], array(
        'query' => drupal_query_string_encode($_GET, array('q', 'program')) .'&program='. $_GET['program'])
      );
      $title .= ' » '. $_GET['program'] .' ';
    }
    $output .= theme('breadcrumb', $breadcrumb);
  }
  else {
    $title   = ' » All';
  }
  drupal_set_title('Syslog Viewer'. $title);
  return $output;
}

/**
 * Actual syslog page callback
 * @return
 *   Themed page content
 */
function syslog_viewer_display() {
  $page_content = ''; // Declare output variable
  $syslog_database = variable_get('syslog_viewer_database', 'default');
  $syslog_table =  variable_get('syslog_viewer_table', 'systemlogs');

  $table_headers = array(
    array('data' => t('Host'    ), 'field' => _syslog_viewer_sortable_link('hostname')                  ),
    array('data' => t('Facility'), 'field' => _syslog_viewer_sortable_link('facility')                  ),
    array('data' => t('Severity'), 'field' => _syslog_viewer_sortable_link('severity')                  ),
    array('data' => t('DateTime'), 'field' => 'datetime'                              , 'sort' => 'desc'),
    array('data' => t('Program' ), 'field' => _syslog_viewer_sortable_link('program' )                  ),
    array('data' => t('Message' ),                                                                      ),
  );

  $sql = 'SELECT `hostname`,`facility`,`severity`,`datetime`,`program`,`msg` FROM `%s` '
       . _syslog_viewer_get_where_clause()
       . tablesort_sql($table_headers);

  // In order top prevent breakage, the "different" database must be held for as little time as possible
  $old_db_connection = db_set_active($syslog_database);
  // Pager query is like db_query_range, except that the start of the LIMIT depends on the page number
  $result = pager_query($sql, 30, 0, NULL, array($syslog_table));
  db_set_active($old_db_connection);

  $rows = array(); // Declare $rows array for table
  while ($row = db_fetch_object($result)) {
    // No sense translating hostname, date or msg
    $rows[] = array(
      _syslog_viewer_get_where_link('hostname',   $row->hostname ),
      _syslog_viewer_get_where_link('facility', t($row->facility)),
      _syslog_viewer_get_where_link('severity', t($row->severity)),
                format_date(strtotime($row->datetime), 'small'   ),
      _syslog_viewer_get_where_link('program',  t($row->program) ),
                                      check_plain($row->msg      ),
    );
  }

  $page_content .= _syslog_viewer_display_text();
  $page_content .= theme('table', $table_headers, $rows);
  $page_content .= theme('pager', NULL, 30);

  return $page_content;
}

/**
 * Implementation of hook_menu for this module
 * @return array
 *   An array of menu items for the syslog_viewer module
 */
function syslog_viewer_menu() {
  $items = array(); // Declare menu array

  $items['admin/settings/syslog_viewer'] = array(
    'title' => 'Syslog Viewer module settings',
    'description' => 'Configure how the Syslog Viewer works',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('syslog_viewer_admin'),
    'access arguments' => array('administer syslog_viewer'),
    'type' => MENU_NORMAL_ITEM,
  );

  $items['syslog_viewer'] = array(
    'title' => 'Syslog Viewer',
    'page callback' => 'syslog_viewer_display',
    'access arguments' => array('view syslog_viewer data'),
    'type' => MENU_NORMAL_ITEM
  );


  return $items;
}