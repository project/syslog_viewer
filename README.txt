
Description

This module allows an administrator to view the contents of the syslog for a 
particular system. The module itself does not actually get the logs from the 
syslogd and put them into the database, this must be done separatly.
Caveat Emptor: currently this module supports _exactly one_ database scheme -
the one I use. Work will be underway when I have the time to generalise this.

Configuration

In order to allow this module to connect to a different database to that used
by Drupal it is, alas, necessary to edit your settings.php file.
The $db_url line must be changed from a string to an array, e.g.
  Old:
    $db_url = 'mysqli://username:password@localhost/databasename';
  New:
    $db_url = array(
      'default' => 'mysqli://username:password@localhost/databasename',
      'syslog'  => 'mysqli://username2:password2@localhost/syslog',
    );
Read the documentation in settings.php for more information.
Once this change is made, the database on setting admin/settings/syslog_viewer
must be changed to match the key (i.e. 'syslog') of the relevent connection in 
this array.

Database Schema

Currently, the module only supports the scheme created by the following SQL:

  CREATE TABLE `syslog`.`systemlogs` (
  `id` int( 10 ) unsigned NOT NULL AUTO_INCREMENT ,
  `hostname` varchar( 60 ) NOT NULL ,
  `facility` varchar( 20 ) NOT NULL ,
  `severity` varchar( 20 ) NOT NULL ,
  `datetime` datetime NOT NULL ,
  `program` varchar( 30 ) NOT NULL ,
  `msg` text NOT NULL ,
  PRIMARY KEY ( `id` )
  ) ENGINE = InnoDB DEFAULT CHARSET = utf8;

N.B. This *will not* be created by the module install.

rsyslog

Since I use rsyslog, I have included an example of an rsyslog configuration
that can be used to put syslog data in this table. See the rsyslog.conf man
page and online html documentation for more information.

  $ModLoad ommysql                  # Load the MySQL output module
  $WorkDirectory /var/spool/rsyslog # where to place spool files
  $ActionQueueFileName mysqlq       # unique name prefix for spool files
  $ActionQueueMaxDiskSpace 1g       # 1gb space limit (use as much as possible)
  $ActionQueueSaveOnShutdown on     # save messages to disk on shutdown
  $ActionQueueType LinkedList       # run asynchronously
  $ActionResumeRetryCount -1        # infinite retries if host is down
  $template systemlogs,"insert into systemlogs(hostname, facility, severity, datetime, program, msg) values ('%hostname%', '%syslogfacility-text%', '%syslogseverity-text%', '%timereported:::date-mysql%', '%programname%', '%msg%')", SQL
  *.info;authpriv.none :ommysql:localhost,syslog,syslog,ThisIsABadPassword;systemlogs

Naturally, if you do not understand what this configuration does *do not* use 
it until you do.